---
layout: handbook-page-toc
title: Fulfillment Backend Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For more details about the product vision for Fulfillment, see our [Fulfillment](/direction/fulfillment/) page.

Fulfillment manages several product categories:

| Category | Description |
| ------ |  ------ |
| [Licensing (license.gitlab.com)](https://gitlab.com/gitlab-org/license-gitlab-com) | Covers all aspects of our licensing model, from how we count seats and conduct true-up to how we count active seats and keep the customer informed on their seat utilization. |
| [Transactions (customers.gitlab.com)](https://gitlab.com/gitlab-org/customers-gitlab-com) | How customers pay for GitLab. Licensing is about how we package GitLab as an offering, whereas Transactions is about how we fulfill those business relationships and how we make doing business with GitLab a great experience for both self-managed and GitLab.com. |

## Team members

<%= direct_team(manager_role: 'Engineering Manager, Fulfillment and Telemetry', role_regexp: /[,&] Fulfillment/) %>

## Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] Fulfillment/, direct_manager_role: 'Engineering Manager, Fulfillment and Telemetry') %>

## How we work

* In accordance with our [GitLab values](/handbook/values/)
* Transparently: nearly everything is public, we record/livestream meetings whenever possible
* We get a chance to work on the things we want to work on
* Everyone can contribute; no silos

### Development workflow

```mermaid
graph TD;
    A(Planning and prioritization) -->|Estimation | B[Ready for development];
    B --> C[In dev];
    C --> D[In review];
    D --> E(Verification);
    E --> F(Closed fa:fa-check-circle);
```

### Planning

We plan in monthly cycles in accordance with our [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline).
Release scope for an upcoming release should be finalized by the `1st`.

On or around the `26th`: Product meets with Engineering Managers for a preliminary issue review. Issues are tagged with a milestone and are estimated initially.

### Prioritization

Our planning process is reflected in boards that should always reflect our current priorities. You can see our priorities for [backend](https://gitlab.com/groups/gitlab-org/-/boards/1072592).

Priorities should be reflected in the priority label for scheduled issues:

| Priority | Description | Probability of shipping in milestone |
| ------ | ------ | ------ |
| P1 | **Urgent**: top priority for achieving in the given milestone. These issues are the most important goals for a release and should be worked on first; some may be time-critical or unblock dependencies. | ~100% |
| P2 | **High**: important issues that have significant positive impact to the business or technical debt. Important, but not time-critical or blocking others.  | ~75% |
| P3 | **Normal**: incremental improvements to existing features. These are important iterations, but deemed non-critical. | ~50% |
| P4 | **Low**: stretch issues that are acceptable to postpone into a future release. | ~25% |

You can read more about prioritization on the [direction page](/direction/fulfillment#how-we-prioritize).

### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. This is normally done in the monthly planning meeting.

When estimating development work, please assign an issue an appropriate weight:

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. |
| 13 | A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. |

Issues that may take longer than 2 days or have a weight of 3 or more, may require additional splitting (either in smaller issues if the requirements can be simplified, or by creating separate MRs for the same issue).

This will ensure that we can reach 10 MRs per dev per month, therefore increasing [throughput](/handbook/engineering/management/throughput/).

### During the release

If a developer has completed work on an issue, they may open the [prioritization board](https://gitlab.com/groups/gitlab-org/-/boards/1072592).
and begin working on the next prioritized issue (First P1s, then P2s, and so on...).

An issue will have 4 stages, and they should be moved accordingly using the [Fulfillment workflow board](https://gitlab.com/groups/gitlab-org/-/boards/1072626)

These are:

* Ready for development
* In dev
* In Review
* Verification

### Quality

The customer portal has different types of tests running:

1. Linting and [rubocop](https://github.com/rubocop-hq/rubocop) jobs
1. Unit tests (specs, these could be of many types, such as controller specs)
1. Integration tests (specs, mocking external calls)
1. Frontend tests
1. E2E integration tests (TBD)

We also have a flag `VCR` that mocks external calls to Zuora by default. We have a [daily pipeline](https://gitlab.com/gitlab-org/customers-gitlab-com/pipeline_schedules) that runs at 9AM UTC with the flag set so the API calls hit the Zuora sandbox and we are notified of any failure (due to potential API changes).

Any test failure is notified to #g_fulfillment_status including a link to the pipeline. Pipeline failures will prevent deployments to staging and production.

### Deployment

We use CD (Continuous Deployment) for the [transactions portal](https://gitlab.com/gitlab-org/customers-gitlab-com/) and a MR goes through the following stages once it gets merged into the `staging` branch:

```mermaid
graph TD;
    A(Merged) --> |Green tests| B(Staging);
    B --> C[Verification];
    C --> D(Auto deploy to production in 3 hours);
```

If something goes wrong at the `Verification` stage, we could create an issue with the label `production::blocker`, which will prevent deployment to production. The issue cannot be confidential.

For MRs with significant changes, we should consider using [feature flags](https://gitlab.com/gitlab-org/customers-gitlab-com/#feature-flags) or create an issue with the `production::blocker` label to pause deployment and a allow for longer testing.

### Feature freeze

The feature freeze for Fulfillment occurs at the same time as the rest of the company, normally around the 18th.

| App | Feature freeze (*) | Milestone ends |
| ---      |  ------  |----------|
| GitLab.com   | ~18th-22nd   | Same as the freeze |
| Customers/License   | ~18th-22nd   | Same as the freeze |

(*) feature freeze may vary according to the [auto deploy transition document](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/auto-deploy-transition.md#transition).

Any issues not merged on the current milestone post feature freeze, will need to be moved to the next one (priority may also change for those).

### Measuring Engineering Throughput

One of our main engineering metrics is [throughput](https://about.gitlab.com/handbook/engineering/management/throughput/) which is the total number of MRs that are completed and in production in a given period of time. We use throughput to encourage small MRs and to practice our values of [iteration](https://about.gitlab.com/handbook/values/#iteration). Read more about [why we adoped this model](https://about.gitlab.com/handbook/engineering/management/throughput/#why-we-adopted-this-model).

We aim for 10 MRs per engineer per month which is tracked using our [throughput metrics dashboard](https://app.periscopedata.com/app/gitlab/559055/WIP:-Backend-Growth:Fulfillment-Development-Metrics).

We also have a general [quality dashboard](https://quality-dashboard.gitlap.com/groups/gitlab-org/sections/group::fulfillment) for the whole Fulfillment team.

### Meetings (Sync)

We hold optional synchronous meetings biweekly on Tuesdays 03:30pm UTC. These meetings are used to make sure the backend team is collaborating effectively and ask any general questions as well as mention any progress updates relevant to Product.

We hold optional synchronous social meetings weeekly, every Wednesday at 03:30pm UTC. In these meetings we chat about anything outside work.

### Retrospectives

After the `8th`, the Fulfillment team conducts an [asynchronous retrospective](/handbook/engineering/management/team-retrospectives/). You can find current and past retrospectives for Fulfillment in [https://gitlab.com/gl-retrospectives/fulfillment/issues/](https://gitlab.com/gl-retrospectives/fulfillment/issues/).

## Common links

 * [All open Fulfillment epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment)
 * [Issue Tracker](https://gitlab.com/gitlab-org/fulfillment/issues)
 * [Slack channel #g_fulfillment](https://gitlab.slack.com/app_redirect?channel=g_fulfillment)
 * [Daily standup channel #g_fulfillment_daily](https://gitlab.slack.com/app_redirect?channel=g_fulfillment_daily)
 * [Team calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_7199q584haas4tgeuk9qnd48nc%40group.calendar.google.com)
