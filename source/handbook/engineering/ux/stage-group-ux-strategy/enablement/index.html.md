---
layout: handbook-page-toc
title: "Enablement UX Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The Enablement area is made up of Memory, Distribution, Ecosystem and Geo. While these are quite different areas with their own Jobs to Be Done, the over-arching theme is making GitLab easier to install, use and and integrate, so that people can get started fast and without friction, and use GitLab where and when works for them. GitLab believes that **everyone can contribute**, and this is central to our strategy.

[See the entire team here](/handbook/product/categories/#enablement-section)

## UX team
<!--Let's talk about who we are and link to our ReadMe's-->

* [Jacki Bauer](/company/team/#jackib) - UX Manager [Jacki's ReadMe](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md)
* [Sunjung Park](/company/team/#sunjungp) - Product Designer, Geo and Distribution
* [Libor Vanc](/company/team/#lvanc) - Senior Product Designer, Ecosystem
* [Evan Read](/company/team/#eread) - Senior Technical Writer
* [Eileen Ruberto](/company/team/#eileenux) - Senior UX Researcher

##### How We Work
We follow the workflows outlined in the UX section of the handbook. In addition,
* we have an [issue board](https://gitlab.com/groups/gitlab-org/-/boards/1254585?label_name[]=UX&label_name[]=devops%3A%3Aenablement) that shows what we areup to. 
* we lable our issues with the UX, devops::enablement, and group:: labels.


<!--
To complete this section, we might answer questions like:
- who are we designing for and what is their desired experience?
- how do we align the desired experience to company strategy
- what is our high level approach and philosophy?
These are hard questions! Not meant to be answered in a day, we can do iterative user research to uncover this information over time. -->

### Our customer
Coming soon! We will answer questions like:
- who are the customers we're selling to?
- what are they like at individual or organizational level?
- what value do we provide them?
- what do they think of our product?
- what improvements do they want?
- how do they interact with the product?
- what are the personas we've created?
- what are their purchase concerns? 
- What is their purchase/use journey?


### Our user
Coming soon! We will answer questions like:
- who are the users we're designing for?
- what are they like at individual or organizational level?
- what value do we provide them?
- how can we best communicate that value?
- what do they think of our product?
- what improvements do they want?
- how can we help them engage more with our product?
- how can we help them learn our product?
- how do they interact with the product?
- what are the personas we've created?
- What is their purchase/use journey?

## Customer Journey
As a team we feel that it's really important to understand, document and consider the entire customer journey during design. To that end we will document what we know as we learn it and link that documentation here.


## Our Jobs to Be Done
Coming soon! We will list the main JTBD, along with links to helpful material such as an XP baseline, usability score, walkthrough video, journey map, recommendations for improvement, etc. Placeholders are good if we know the JTBD but haven't worked on it.

#### JTBD for Geo
TBD

#### JTBD for Distribution
TBD

#### JTBD for Ecosystem
TBD


## Our strategy
The Enablement UX team will be work with Enablement PMs to uncover customer's core needs and workflows. Becoming strategic involves gathering research, looking for patterns, and making plans for the best path forward for our customers and users. It is also about deciding what we value most, and how to best work together to achieve our goals.

<!-- Copied from Secure's strategy list and made a few changes for our team. I think a lot of what they say applies to us, but am open to all suggestions!-->
We are doing activities like this in order to build our strategy:
Our approach to this work includes:
* [Baseline initiative audit and recommendations](/handbook/engineering/ux/experience-baseline-recommendations/) (quarterly)
* Internal understanding: stakeholder interviews (annually)
* Customer understand: user research (ongoing)
* Supporting Growth team experiments
* Rapidly prototyping in order to test ideas
* Performing heuristic evaluations on at least 3 competitors, based competitors the 3 user type is using (annually, ongoing)
* We talk to our customer (ongoing)
* We talk to our users (ongoing)
* We outline current user workflows and improve them (upcoming, ongoing)

Additionally, we value the following:
* A Lean UX approach that entails making a hypothesis, using data and experiments to test the hypothesis, implementing winning ideas, and iterating.
* Testing our features with usefulness and usability studies
* Partnering closely with our internal stakeholders in Legal, Support, Finance and Marketing for feedback and feature adoption
* Partnering with our sales and account team to connect directly with customers and learn why customers did (or didn’t) choose our product
* Billing, settings and other functionality that is secondary to the product should work just as well as the product.
* Prioritizing issues that are likely to increase our number of active users

