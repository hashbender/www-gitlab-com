---
layout: handbook-page-toc
title: "UX Research Training"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Designing user research

### How to choose a research methodology

When choosing the appropriate methodology for your research question, there are multiple factors to consider: the need to understand the details of an issue versus its frequency of occurring, the characteristics of your users and their usage, and the fidelity of your product or designs. Each of these factors will dictate which methodology is most appropriate for your research questions. We'll go into each of these factors in more detail.

#### Detail versus frequency

The first factor to consider is whether you are just starting out and wanting to get an idea of what problems may exist, or do you already have an understanding of the problems users are facing and you're looking to understand their severity or relative frequency?

If you're in the initial discovery phase, it's important to be able to get into the weeds on your problem. You'll want the ability to ask detailed questions, follow up on participants' answers, and deviate from your discussion guide when interesting tangents present themselves. Thus, in-depth interviews are likely going to be preferable rather than a survey or other method where you have a fixed set of questions and no ability to follow up.

Once you have an idea of the types of issues your users are experiencing, you may want to understand how prevalent those issues are with the wider population. To do that, you need a suitably large sample of users so that you can be reasonably confident that your findings are representative of the user population as a whole. In this instance, a survey that you can distribute to potentially hundreds or thousands of users is a better fit. Using a survey with a fixed set of answer choices will allow for easy analysis of even a large number of responses.

#### Characteristics of your users

The next thing you'll want to think about when devising your study is the characteristics of your users' behavior or specific actions you're looking to study. Do all people do the thing in the same way, or does it vary, perhaps based on their role? Take for example administrators versus regular users. Their understanding and usage of the product will likely be very different. In that case, you would want to be sure you recruit people in different roles. 

Does usage change over time? Perhaps you're interested in studying how people learn to use a new feature. It might be a good idea to talk to users at multiple intervals as they learn the feature, or at least recruit users at different points on the spectrum of usage.

The important thing is to think about how usage might be different for people based on who they are or how they're using your product and try to recruit people that reflect that different usage.

#### Fidelity of insight

People often ask at what stage of product development they should be conducting UX research. The reality is that you can conduct research at every stage of development with different levels of detail. The fidelity of research insights follows the fidelity of design. 

If you have a hand drawn product flow on some pieces of paper, you can put that in front of users and get a basic, high level idea of how they comprehend the experience. The key phrase being *high level*, you won't get any detail and you shouldn't try. If you then turn that flow into a high fidelity prototype where users can click "hotspots" on a series of screen images to progress through the flow, you can get more detail, but still not as much as if they were interacting with a  production interface.

The important thing to keep in mind is that you can do research at every stage of the development lifecycle, as long as you remain mindful of the limitations of what you can learn. However, even with these limitations, doing research at early stages is immensely valuable for providing product direction. Some information is so much better than no information.

## User Interviews

### How to write a strong hypothesis

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/i-LIu-zDOOM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### Video transcript

When you hear a customer problem, it can be tempting to just dive right in and devise a solution. However, it’s important to remember there is never just one good solution to a problem. A problem can be solved in many different ways, depending on what we need to focus on. 

Users can also be unpredictable, what we think might solve their pain points, may not actually even begin to address the problems they are facing. Therefore, it’s advisable to test your ideas before you start building a solution. A way in which you can do this is to write and test hypotheses. 

A hypothesis is basically an assumption. It’s a statement about what you believe to be true today which can be proven or disproven using research.

A strong hypothesis is usually driven by existing evidence. Ask yourself: Why you believe your assumption to be true? Perhaps your hunch was sparked by a passing conversation with a customer, something you read in a support ticket or issue, or even something you spotted in GitLab’s usage data.

There are lots of different structure for hypotheses, but I recommend using this simple statement:

> We believe [doing this] for [these people] will achieve [this outcome].

The statement is comprised of three elements.

The first part: `We believe [doing this]` should detail your proposed solution to users’ problems.

The second part: `For [these people]` should identify who you are targeting.

The third and final part: `will achieve [this outcome]` is where you should document your measure of success. What is your expected result?

For example:

We believe `storing information about how an incident was resolved, how long it took to resolve and what the outcome was in a historical record` for `engineers responsible for incident management` will achieve `a 20% faster resolution time for incidents`. This is because referring to past incident information helps to inform potential solutions for remediation.

When writing your hypothesis, focus on simple solutions first and keep the scope small. If you’re struggling to articulate your assumptions about users, it’s probably better to start with developing a better understanding of users first, rather than forming weak hypotheses and running aimless research studies.

A strong hypothesis is easy to test. It shouldn’t take you much time to design a research study to validate or invalidate your hypothesis. 

If your hypothesis is invalidated by users, don’t feel disheartened. You’ve stopped precious Engineering time being spent on building a solution that simply doesn’t solve users’ problems. A good measure of being iterative is throwing something away because user research proved that it wasn’t going to work. You’re not always going to get things right the first time. We learn more about user needs as a result of testing multiple hypotheses and, in turn, we generate new ideas for future rounds of testing.

### How to recruit participants

There are three phases of recruiting: identifying your target participants, crafting your screening survey (screener), and opening a recruitment request issue. A researcher or coordinator may collaborate with you throughout the process, and we’re always available to answer questions. [Check out a recruiting case study here](/handbook/engineering/ux/ux-research-coordination/#recruitment-case-study) for examples of each phase.

#### Identify target participants

- Who do you want to speak with? 
- How many participants do you need?
- What’s your ideal research timeline? 

The answers to these questions should flow directly from the methods and goals that you’ve previously identified for your study. It’s important that everyone agrees on these and that they’re finalized before we start recruiting. It’s better to start with a clear objective than to try to shoehorn mistakenly recruited participants into a study with different aims. 

Be aware that some of your criteria can also be hypotheses — for example, sometimes we think of job title as a proxy for tasks and responsibilities that we think a role comprises. But, there may be people who have a different job title but perform all the tasks and responsibilities you’re interested in. One should be open to different ways of identifying your target participants.

#### Craft your screener

The screener has a specific function - it’s meant to identify the people who are your target demographic, so that you can ask them the things you really want to know in the study. Team members often draft these and collaborate in Google Docs. Once finalized, you should enter the screener into Qualtrics. 

There should be a 1:1 match of your questions to each criterion you have, so that a coordinator can look at your desired criteria and know which answers you want to see on the screener. The more abstract or open-ended you get in the screener, the harder it is for coordinators to parse which answer it is you’re looking for. 

Common questions we include are:
- Consent to record the session
- Consent to store the recording in GitLab
- Job title
- Industry
- Team and company size
- Common tasks
- Tools 

There are copies of these questions with standard phrasing in the UX research team library in Qualtrics. 

It’s important to remember that a screener is not the same as a survey. There can be a temptation to pile on extra questions that you want to know the answer to, but aren’t strictly necessary for determining whether that participant should get through the door. You may benefit from including these in your warm-up questions at the start of your session instead. Straightforward factual questions are easier for the participant to answer and can help you both relax. 

If you have many questions like this before an interview or usability test, you may need to consider doing a survey instead, to learn more about your users before in-depth sessions.

#### Open a recruitment request

Open a `Recruiting request` issue in the [UX research project](https://gitlab.com/gitlab-org/ux-research/issues) and be sure to @ mention the relevant coordinator. They will work with you to clarify any of your criteria and do a sense check of your screener. 

Once your screener is finalized and in Qualtrics, the coordinator can take action. Depending on your desired participants and timeline, they might send an invitation to our research panel, GitLab First Look, open a campaign via a third-party recruiting service, request social promotion from the marketing team, suggest a blog post and newsletter placement, ask other team members for introductions to users, or try other tactics. 

Recruitment may only take a few days if your target users are plentiful -- developers, devops professionals, etc. Recruitment can take longer for low-incidence users, like security professionals or users of a newly released feature. Generally, two weeks is enough time to recruit even difficult-to-reach users. In rare cases when your target users cannot be found, the coordinator will suggest other options such as relaxing your criteria. In all cases, the coordinator will keep you updated on the progress of the recruitment effort by commenting in the recruiting issue. 

The coordinator will share the screener results and suggest which users best fit your criteria. They will work with you to configure your Calendly, and will invite users to schedule with you. You can work asynchronously by noting when users have scheduled with you and when sessions have taken place. The coordinator will send thank you gifts to participants who have completed sessions. Once all participants have been sent gifts, the recruiting request can be closed. 


#### Written consent to record

* Collect the participant's response in a survey (Qualtrics).
* Use single select checkboxes.
* A participant must select 'I agree...' in order for you to be able to record the conversation.
* Verbally double check permission to record at the start of your user interview.

```
What you say and do during a research study is really important to GitLab. We create issues to resolve the problems we witness during a study. To make sure our issues correctly represent what you say and do, we would like to record:

(1) the conversation you have with our [researcher/designer/product manager/team]

(2) anything you choose to share on your screen with us during the study 

Please indicate below whether you give your permission to be recorded.

* I agree, I give my permission for my voice and screen to be recorded.

* I disagree, I do not want my voice or screen to be recorded.
```

#### Written consent to share

* Collect the participant's response in a survey (Qualtrics).
* Use single select checkboxes.
* A participant must select 'I agree...' in order for you to share the recording publicly on GitLab. 
* Verbally double check permission to share at the start of your user interview.
* Recordings should be stored on Google Drive and permissions set as appropriate.
* If the participant agrees to be recorded, but doesn't agree for the recording to be shared, ensure the permission on the video is `On - Anyone at GitLab with the link`.
* Please use common sense. If a participant agrees for the recording to be shared publicly (`On - public on the web`) but the recording includes either sensitive information or could be defamatory to an individual or organization, please refrain from sharing the video (use the permission of: `On - Anyone at GitLab with the link`).

```
At GitLab, we value transparency. By making information public, we can reduce the threshold to contribution and make collaboration easier. We would love to share the recording of the research study on GitLab. This is completely voluntary and up to you. 

Please indicate below whether you give your permission for the recording to be shared on GitLab.

* I agree, I give my permission for the recording to be shared on GitLab.

* I disagree, I do not want the recording to be shared on GitLab.
```

### Communication

Below are email/message templates that you can use for communicating with research participants. Whenever possible, we strongly encourage you to customize each message for the individual and circumstance.

#### Invitation to partake in a user interview, following successful screening

```
Hi! We have a study coming up, and I was wondering if you might be interested in participating with us. Based on your response to our survey, you look like a great fit! Sessions are taking place from `[XX-XX]`, and they last about `[XX]` minutes over Zoom (videoconference).

For this round of testing, we’ll be chatting about `[Replace with research subject. Example: What tools you use, what your process is like, etc.]`.

Participants who complete a session will be compensated with a `[Replace with compensation. Example: $60 Amazon gift card, or approximate value in your home currency]`.

If you are interested, go here `[Link to your Calendly]` and choose one time and day that works for you as soon as possible. There are limited spots available.

Please let me know if you have any questions.
```

#### Outreach internal customers (GitLab) in Slack

```
Hi all! :wave: We are in the process of `[Replace with research subject]` to `[Replace with research goals for context]`.
We need internal customers to answer a few questions. If you would like to help us out, please reply to this survey `[Link to research survey]`. Thank you!
```

### How to write a discussion guide

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/sXE8F5Vu1sA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### Video transcript

A discussion guide is a set of questions and topics that you would like to discuss with a participant during a user interview. It typically consists of an introduction, warm-up questions, exploratory questions and a debrief. Today, I’m going to walk you through how to create a discussion guide.

**Introduction**

Introduce yourself and let the participant know what to expect during the interview. Give them a chance to ask questions. Most people won’t have been interviewed before so take some time to put them at ease. Prior to the interview, you should have already obtained written consent to record and possibly share the conversation you have with the participant. However, it’s a good idea to double check verbally that the participant is still happy to be recorded and for the conversation to be shared.

**Warm-up questions**

Start by asking the participant a few easy questions about themselves and their job. This will help the participant get used to the process of answering questions. It’s also an opportunity to begin building rapport with the participant, so that they are more inclined to open up to you when you begin asking exploratory questions. Listen closely, their answers may help provide context for any later responses they give. Some warm-up questions you could ask are:

* On the screening survey, you mentioned that you are a DevOps Engineer. How long have you been in that role?
* What kind of work does your company do?
* What does your typical working day involve?

**Exploratory questions**

When you start writing your exploratory questions, you’ll want to group questions into common topics, so that your interview naturally flows. As you begin to structure your questions, allocate time for each topic. This will help keep your interview on track. Move from general questions to more specific questions related to your research goals. For example ‘How do you currently go about this task? to ‘What’s the hardest part about task?’ to ‘What could be better about how you currently do this?’. At the same time, don’t leave your most important questions until the very end in case a user spends more time than you anticipate answering an earlier question.  

It’s okay to ask questions about past experiences, as long as you recognize the limitations of people’s memory. The human memory is fallible and it can often be difficult for people to remember specific details. For example, if I asked you whether you had breakfast three days ago? You could probably tell me yes or no. Yet, if I asked you to recall how long your breakfast took to eat, you’d probably struggle to provide an answer or you might even be tempted to hazard a guess. Ask questions which delve into participants’ general experiences and opinions but don’t press participants for details they can’t provide. Otherwise, they may be tempted to make up their answers.

Participants can’t predict the future. If you ask them a question like: 'Would you use this feature?' their response may not be an accurate reflection of what they would actually do. For example, some people might say ‘No’ because they might not be able to visualise how the feature would work from a description alone. Others might say ‘Yes’ because they don’t want to rule out the possibility that at some point in the future the feature might be useful to them.

**Debrief**

Thank the participant for their time and explain what happens next with the feedback they have given you today. Give the participant a chance to ask any questions. If you are paying a participant for taking part in your study, ensure you share details of how they will be paid and when they can expect payment. Leave your contact details with them in case they have any follow-up thoughts they want to share with you.

Once you have written your discussion guide, you should rehearse and test out your guide, this can be with a colleague. This will give you a sense of how long your script will take to run through and it will help you spot any questions that people may have difficulty answering. 

Remember your discussion guide, is just that, it’s a guide. It’s a reference tool which helps facilitate conversation. If a participant says something interesting, which is not covered by your guide, listen to them and explore what they are saying. You may uncover something you hadn’t previously considered. Active listening is key, you should react to what your participant is saying.

### Discussion guide template

[View the discussion guide template](https://docs.google.com/document/d/1wIzpVTduHii1AN-tHFgKME1H-ellDxoKc7IT47yZ_tk/edit?usp=sharing)

### Example questions for user interviews

* Ask open ended questions to keep a conversation flowing. 
* Remember to probe participant’s answers. Ask: Why? Why not? Who? What? Where? When? How?
* Never ask participants what they want.
* Don't use all the questions below! Select a couple to use.

#### Warm-up questions

* What's your current role?
* What made you become a [job title]?
* How long have you held the role of [job title]?
* What kind of work does your company do?
* What does your typical working day involve?
* What would you say are your top 3 tasks?
* How long have you used GitLab?
* What do you use GitLab for?

#### Exploratory questions

* What are you trying to [achieve/get done]? Why?
* How do you currently go about this [task/process]?
* Can you show me how you currently do this?
* Has anything about the way you do this changed over time? How? Why?
* Do you anticipate [task/process] changing? Why?
* What do you like about [task/process]?
* What’s the hardest part about [task/process]?
* What could be better about how you currently do this?
* Tell me about the last time you tried to [task/process]
* Why do you keep doing [task/process] Why is it important to you? 
* How often do you do [task/process]?
* How often do you experience [problem]?
* What happens before/after you experience [problem]?
* What work-arounds have you implemented to help you with this?
* What are you currently doing to make this [task/process] easier? 
* Are you looking for an alternative solution for [task/process]? Why? Why not?
* How does this [task/process] impact other areas of your work?
* Could you describe step by step how you do [task/process]?
* What step is the most time-consuming? Why is that?
* If you could remove one step from the process, which step would it be?
* What other products have you tried? 
* How did you hear about these other products? 
* Why did you decide to try [product]?
* What made you choose [product] in the end?
* What do you like about [product]? 
* What do you dislike about [product]?
* What’s the hardest part about using [product]? 
* How does the cost of [product] compare to the value it provides?
* How important is [product] to your organization? How would you rate its level of important on a scale of 1 to 10 with 10 being most important?

### Tips for facilitating user interviews

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/-6U5p6A4WWE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### Video transcript

**Build rapport**

When you conduct an interview, it’s crucial that you are able to build rapport with your participants. People are more likely to talk and let their guard down if they feel relaxed. The quality of the interview and the data you collect will suffer if you are unable to earn a  participant’s trust. While it might sound obvious you should: greet participants by their name, smile - since a positive mood is contagious, be friendly and initiate small talk before transitioning into your interview.

**Let the participant do most of the talking**

You should avoid talking about your own opinions. If you share too much of your own experiences, you risk influencing your participant’s answers. They will be less forthcoming and open if they disagree with your opinion, this may lead them to skew their answers and you’ll end up with inaccurate data.

Silence during interviews is sometimes hard to deal with. As tempting as it is to talk during these awkward moments, it’s actually better if you give participants the opportunity to fill these gaps. 

Silence is a natural and important part of user interviews, it allows participants to pause and gather their thoughts. It gives them the sense that you’re waiting for them to say something and it usually encourages them to speak their thoughts out loud. By jumping in and filling that gap, you might interrupt a participant’s thoughts and miss out on a key insight.

**Remain neutral while demonstrating empathy**

Remaining neutral is something that takes most people a lot of practice. When a participant has experienced a difficult or frustrating situation, our natural instinct is to empathize with them. However, we need to act sympathetically without leading the participant or making assumptions. 

For example: Imagine a DevOps Engineer tells you that he or she is responsible for incident management. They’ve had a rough week. They’ve been frequently woken up in the middle of the night to attend to incidents. 

As an empathetic human being, your natural reaction may be something like: “That must have been really frustrating for you!” but that would be leading the participant. Instead, you could show some concern by asking the participant to elaborate: “Can you tell me more about that?”. 

You could even try a question like “How did that make you feel?” but only if the user hasn’t already indicated how he or she felt. By asking a question that relates to the participants’ feelings, you can show that you are listening and that you empathize with their situation.

**Be an attentive listener**

Turn off your desktop notifications. Close down the million tabs that you have open and leave your phone in another room. It is crucial that you are not distracted during an interview.

Make the participant feel heard by nodding, looking at them directly through your camera and offering acknowledgments like “Hmm” and “I see”. Always let participants finish their thoughts. Do not interrupt them unless absolutely necessary.

The better we listen, the better data we can gather. Attentive listening is really important because participants take time out of their day to talk to us. It’s just plain good manners to give them our full attention and make them feel like they’re being heard.
 
**Be curious**

Even if you think you know the answer to a question - ask the question anyway. It’s not about what we know, it’s about trying to understand what the participant has to say on the subject. We need to be mindful of our own biases and assumptions and remain curious. Also, don’t assume participants wouldn’t know the answer to a question or will provide a poor response. Ask the question any way and see what they have to say.
 
**Don’t lead users**

A common concern that most people have when conducting user interviews is unintentionally leading a participant. If a participant says something that is unclear to you or that you want to follow-up on and you can’t quite find the right words on the spot. A simple technique is simply to repeat back what the participant has said with some intonation. 

For example, imagine a participant said:

> “The interface isn’t intuitive”

The facilitator could say:

> “Isn’t intuitive?”

This is especially useful when a participant uses a buzzword like “intuitive”. It’s important to dig into what the participant actually means when they use a word like this. As mentioned earlier, we must be mindful of using our own assumptions to interpret the meaning of “intuitive”. This simple technique encourages participants to continue talking, without unintentionally influencing their response.
 
**How to keep a user interview on track**

As a moderator, it’s your job to keep the interview on track. Most participants are thrilled to speak to someone from GitLab and are keen to share their pain points and concerns surrounding the product. However, sometimes participants digress from the topics you want to discuss. Veering off-topic isn’t necessarily a bad thing. It only becomes problematic when it goes on for too long and isn’t satisfying the study’s objectives.

When this happens, politely interrupt the participant and say: “this is really interesting but I’m conscious of the time we have together today. There’s some other things I’d like to cover with you. Why don’t we move on and return to this a little later on”.
 
**Capture consistent data**

Let’s say you’ve conducted around 2-3 user interviews and, so far, you feel you haven’t begun to capture the data that you need. It’s very tempting to introduce new questions halfway through a study, but this will make synthesizing your data incredibly difficult.

When we synthesize data, we are looking for patterns in responses, this can be done by making sure we ask the same set of questions to every participant. 

Having a certain insight from a single person when the other participants did not get a chance to share their thoughts can create inconclusive results - we don’t know if the insight is only relevant to that one person or whether other participants share the same opinion.

Instead, take a break from interviewing participants, and take some time to review your discussion guide. Remember, you can always reach out to a UX Researcher for advice. If you amend or introduce new questions in your discussion guide, then you will need to restart the process of interviewing participants from the beginning.
 
**Speaking fast and slow**

Participants come from a wide range of backgrounds and their experiences can shape the depth of their answers. Some participants will speed through questions while others will take longer to ponder the question before they reply. That’s completely natural.

For participants who speak fast. Talking slowly to them can have a calming effect. It indicates that you are not anxious and that you have the time to listen to them.

For participants who speak slowly. As long as they are giving you good answers, don’t hurry them. Putting pressure on them could mean you lose out on discovering key insights. 
 
**And finally, be mindful of the time**

Usually, time goes by very fast during interviews. Be respectful of the participant’s time and ensure you end the user interview at the time you have agreed.

### Printable moderation technique cards

[View moderation technique cards](https://drive.google.com/file/d/1kIgDC2N-jhPeuqKjUlvD9DtsDFrGDw23/view?usp=sharing)

### Note-taking template

[User interview note-taking template](https://docs.google.com/spreadsheets/d/1hnIqg-fnCYW2XKHR8RBsO3cYLSMEZy2xUKmbiUluAY0/edit#gid=0). Used to collect raw observations on the participant’s behavior and emotional feedback to the questions, as well as take notes. You can also create a new file directly from the template on Google Drive, by selecting it from one the default styles.

## Usability testing

### How to write a usability testing script

#### What is a usability testing script, and why you should definitely have one
Usability testing is a systematic approach for testing whether users can carry out scripted tasks successfully, finding what their preferences for completing a task are, and uncovering opportunities to improve the product.   
At the heart of a usability study stands the script, which the moderator follows during test sessions.

A script:
* Helps to make sure you will be covering what you set out to cover - your research objectives - and that you’ll come out of the study with the answers you need to make progress on your project.
* Keeps you consistent between sessions, making sure you ask the same of all of your participants. This consistency is important for a solid methodology, and will also make your life easier once it’s analysis time.
* Assists with collaboration, and particularly with letting others review your work.
* Helps you keep time during sessions.

It is virtually impossible to practice solid usability research without having a script. Don’t go in without one.

#### How to write your first draft
Use this template as your starting point for writing a first draft for your usability study:  

* [Usability Testing Script template](https://docs.google.com/document/d/1_5Qu2JR9QE5LE6cK4eq9yJs-nXv2rlWWifcjacaiWdI/edit?usp=sharing).

A usability script typically follows 4 main parts:
1. Introduction
2. Warm up questions
3. Tasks
4. Wrap up questions and closing words

Here is a bit about each part (you may want to read this while going over the template).

##### Introduction
This is where you introduce yourself and other attending members of the team. You also tell the participant how the session is going to go, and double check that you have their consent for recording and sharing the session.  

Use this part to build rapport with the participant. People are often hesitant, nervous or even a little standoffish at the beginning of a research session. Simple comments such as "Oh I've been to where you live and I loved it" can go a long way to making people feel comfortable and the study to run smoother.

##### Warm up questions
Warm up questions are meant to further break the ice as well as getting relevant background information on the participant.

Here are some standard warm up questions to consider:
1. What’s your current role?
1. How long have you been in that role?
1. Do you have experience dealing with X ?
1. Have you ever used GitLab? If so, what for?

**Tip**: Even if you don’t have anything you want to ask, have at least 2 quick questions here, as it will help to break the ice and make the participant feel more at ease. 

**Tip**: Consider asking questions that will help you to understand the participant’s mental model and expectations, prior to interacting with your designs (e.g. "We have a page called Security Dashboard. What tasks would you expect to be able to accomplish with the help of that page?".)

**Tip**: If needed and you have the time for it, you might want to include some more interview-y questions here that would benefit either this study or a different one (if it’s a different study, make sure it shares the same participant profile with this one!). In particular, consider asking questions that might be used later on as part of a persona or a JTBD study (e.g. "What would you say are your top 3 tasks?").

##### Tasks
This is the heart of the script, and the part that takes the longest to write. Usability studies usually consist of 3-4 tasks (though your mileage may vary).

**How to define good tasks**  
When sitting down to write your tasks you should already have your research brief for the study more or less done (to learn more about writing a research brief, see `How to define goals, objectives, and hypothesis in order to ensure proper research focus and definition`. To form good usability tests, start by going over your research objectives (which detail what you and your stakeholders want to get out of the study), and consider how to best translate them into user tasks.

The key thing to remember when moving from objectives to tasks is that **your tasks should reflect realistic user goals**.

For example, perhaps one of your objectives entails finding out whether users can quickly locate a CTA. But since no user ever goes into a website with the goal of locating a button, your task should never be “Can you find and click the button?”. Rather it should be about why the participant might want to click the button in the first place (e.g. “You want to enable feature X for your project. Can you do that please?”).
	
Another objective of yours might be identifying potential improvements to the flow. Since real users don't generally go into websites with the sole purpose of finding faults in it, instead of asking the participant “Can you please complete the following steps and tell me what we should improve?”, ask them to perform a task that would necessitate them to attempt to go through that flow and observe them carefully to see where they fail, struggle or hesitate.

Finally, pay close attention to how you phrase your tasks to avoid bias, leading the participant, and other common pitfalls. To learn more about writing good tasks we highly recommend going over this helpful NN/g’s article:
[Write Better Qualitative Usability Tasks: Top 10 Mistakes to Avoid](https://www.nngroup.com/articles/better-usability-tasks/).

**How to structure each task**  
For each task, consider whether some set up is required to provide context and appropriate motivation for the participant. If so, describe a relevant scenario prior to giving the task. For example:  

Scenario: “Let’s say this is a project you’re working on, and you just committed some new code”.  
Task: “Please test to see whether that code contains any security vulnerabilities”.

Then, consider adding some more specific questions and prompts that the moderator can utilise as the task unfolds, in case the participant isn't bringing these topics on their own. Examples:
* What are you seeing on this page?
* What is this page about?
* Is SAST running right now?
* What would you do next, if anything?

**Tip**: For each task, add a link in your script for the prototype / webpage that’s relevant for that task. Not only will it help your teammates who will be reviewing the script to understand what the task is about, but it will allow you to quickly resend the relevant link should the participant need it again.

**Tip**: Consider noting under each task, in light gray, ‘What we expect them to do’ (e.g. "Follow the CI pipeline and go into the SAST job output"), to remind the moderator of the possible paths for completing the task. This would assist the moderator in helping participants to recover, in case they failed a task which is a prerequisite for the following task.

**How to order your tasks**  
* If your tasks can build on top of each other in a sensible way, make sure you order your tasks to reflect that. For example, Task 1 could be around navigating to a certain page, and Task 2 around reviewing that page.
* Cluster together tasks that belong to the same topic or area of the product.
* As a rule of thumb, start with the tasks that matter most to you. It will take time mastering moderating usability sessions, and it is common to fall behind on time when you’re just starting out. Therefore, start with what matters most to you and leave what’s merely nice to have to the end of the test.

##### Wrap up questions
Here you can get the participant’s broad impressions about what they saw and experienced. Here are some standard questions to consider:
1. What do you think about this process you just went through?
2. How does what you just experienced with GitLab fare in comparison to the tool you normally use?
3. Anything else you’d like to add?

Conclude the script with thanking your participant and mentioning when they are expected to get their compensation.

#### You've completed your draft script. Now what?

##### 1. Review your draft
Once your draft is more or less done, give it another read and ask yourself:
1. Does it cover the project’s objectives?
2. Does it make sense time-wise? Estimating time will get easier with experience. Keep in mind that usability tests should normally take between 30-45 minutes.

##### 2. Let others review your draft
Edit as needed based on feedback received from your stakeholders / teammates.

##### 3. Test the test
Run a dummy test with a colleague / an internal participant to make sure your task instructions are clear and that you’re keeping time. Edit as needed, and notify your stakeholders of any big changes.

### A crash course on remote, moderated usability testing

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/5MvpxvN9vLU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**Slides**

[View the slides](https://drive.google.com/file/d/1AqmTX0atvxRsag5EIdyFduGRfzNQLjRC/view?usp=sharing)

# Analyzing and synthesizing user interview data

It can be tempting to complete a set of interviews or usability tests and think your research project is done. But without a process for systematically reviewing and making sense of the data you've collected, you risk leaving valuable insights on the table. The next step is to zoom out and look at your findings anew through a process called **research synthesis.**

Synthesis helps us take all of the info we've collected, organize it into patterns and themes, and translate those themes into actionable insights. At the end of this process, you should have answers to the core research questions you set out to answer and evidence that proves or disproves your [hypotheses](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/#how-to-write-a-strong-hypothesis).

Below are some guidelines for how to set yourself up for successful analysis and synthesis before you begin talking with users, while you're conducting research sessions, and after your interviews are complete.

## Before interviews: Decide on a collection method

Conducting user research can result in a pretty hefty amount of information. To stay organized throughout this process, spend some time up front deciding where you'll collect your notes. Collect all your research notes for a study into a single Google spreadsheet. Doing this makes it easier for your team to collaborate by adding their own notes and observations, and collecting everything in a central place will keep things organized and consistent.

You should also familiarize yourself with the format of our [UXR Insights repo](https://gitlab.com/gitlab-org/uxr_insights). This will help ensure you've gathered all the information you'll need when it comes time to [document your findings](#) there.

Here are some templates you can use, depending on the type of study you're conducting:

- [User interview note-taking template](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/#note-taking-template)
- [Usability testing rainbow chart](https://docs.google.com/spreadsheets/d/1bPg6op9Sk46lFVGaET-fruE0qz-ctNQsxbZKF-5lpn4/edit#gid=0). This approach uses a templated and color-coded spreadsheet to record what participants did during the test. For a thorough walkthrough on how to use this method, check out [this article](https://userresearch.blog.gov.uk/2019/09/13/how-a-spreadsheet-can-make-usability-analysis-faster-and-easier/), or watch [this video of a GitLab researcher's experience using this method](https://drive.google.com/file/d/1fYRTmaHZjMwDQfAnVpaEqHP1dByy1X5x/view?usp=sharing) (starts at 7:00).

## During interviews: Gather data and discuss with your team

While capturing your research data, you'll focus on documenting **observations** (what you saw the user do, or what problems you saw/heard them experience) and **quotes** (the verbatim of what the user actually said, in quotation marks). You may also want to make notes on early **interpretations** (what you believe something a user said or did means) and possible **solutions** (concrete ways to solve the problems identified).

While the bulk of research synthesis happens after you've finished gathering all your data, analysis can start right away. After every user interview, de-brief with your teammates who observed the session about what stood out.

Capturing these insights while the session is fresh in your minds makes the overall process much faster and easier. Discussing observations as a group also leads to more thoughtful analysis, reduces [cognitive bias](https://medium.com/better-humans/cognitive-bias-cheat-sheet-55a472476b18), and helps your team form a strong shared understanding of the problem space you're investigating.

## After interviews: Find patterns and themes

### Synthesizing user interview data

Affinity diagramming is a way of finding themes in a collection of ideas, quotes, or observations. This method helps you draw out insights from qualitative data quickly and collaboratively. This is traditionally done in person with Post-It notes on a large blank wall or whiteboard. At GitLab, we use a tool called [MURAL](https://mural.co/) to recreate this experience remotely.

#### 1. Prep your notes

Spend some time cleaning up and organizing your notes before moving them into MURAL. How you organize your notes in MURAL is up to you and your project goals. Some teams will start by organizing their notes by user, or by interview questions.

Next, copy and paste your notes into MURAL. Each cell in your notes spreadsheet should paste into MURAL as a separate sticky note.

#### 2. Cluster the data into themes

Once all your individual data points are in MURAL, begin by grouping similar stickies together. You may want to start by grouping stickies for themes that stood out to you from your post-session debriefs, then move to surfacing themes that may not have been as obvious the first time around.

Here are some potential ways to group findings:

- **Equivalence:** e.g., "This finding is the same as this other finding."
- **Association:** Around the same area of the experience being analyzed, e.g., "This finding is best considered at the same time as this other finding."
- **Hierarchy:** Larger thematic trends which several findings support, e.g., "This finding is an example of this other finding."

This process might feel messy, especially if it's your first time doing an affinity diagram. To add more structure to the process, use your original project goals, hypotheses, and research questions as a guide during synthesis. You can copy these right into your MURAL board so you can keep them top-of-mind while you group individual findings into broader themes. You might also reference the [questions listed in this article](https://www.smashingmagazine.com/2013/09/5-step-process-conducting-user-research/#5-synthesis-answer-our-research-questions-and-prove-or-disprove-our-hypotheses) to ask yourself and your team throughout the synthesis process.

As your groupings start to come together, use a text heading to write titles or catch phrases that summarize each cluster of similar stickies.

#### 3. Discuss and revise as needed

Organize and reorganize your findings into meaningful categories until everyone seems to be in agreement. See if you need to adjust any of your themes before moving onto the next step. 

This is also a good opportunity to check for possible bias. Try “[arguing the other side](https://uxdesign.cc/how-to-look-at-evidence-and-not-translate-it-into-your-own-agenda-9860171b7ba9).” In other words, building a case—from your research—*against* your key insights, to see if they still stand up.

#### 4. Distill findings into insights

Once you're happy with your groupings, distill your findings into a manageable number of [insight statements](http://www.designkit.org/methods/62). Insights you uncover should come from multiple sources in your research. Depending how much research you did, the number of insights you uncover may vary.

Once your synthesis is complete, [document your research findings](#) in the [UXR_Insights repo](https://gitlab.com/gitlab-org/uxr_insights).

## Documenting research findings in the UXR_Insights repo
The [UX Research Insights repository](https://gitlab.com/gitlab-org/uxr_insights) is a record of all the user insights discovered through GitLab’s UX research program. User insights can be gathered through methods such as user interviews, usability testing, surveys, card sorts, tree tests and more. 

### Why did we create this repository?
The UX Research team has [always faced challenges](https://about.gitlab.com/blog/2019/07/10/building-a-ux-research-insights-repository/) in finding the best way to create research reports that are easy to digest and access. When using methods such as PDFs, Google docs, and even GitLab CE issues themselves, it was difficult to track and share study findings. Additionally, since we are often asked to readily recall information we've learned in prior studies, it can be tedious to read through old reports, look through pages of interview notes, or rewatch video recordings to find the information we need. This problem compounds, since we are continuously producing research reports and the wealth of information grows infinitely.

The goal of this repository is to make research findings searchable, concise, and easy to reference. It also gives us the opportunity to [dogfood](https://about.gitlab.com/handbook/values/#dogfooding) GitLab as a way to understand what users may experience, if they use GitLab for similar purposes. 

### What is an insight? 
When we conduct research studies, we have a set of [goals and objectives](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/#video-transcript) that guide us as we investigate the research question. Whether we're evaluating the usability of a feature, conducting exploratory interviews, or gathering quantitative data, the research study will likely result in a list of observations, patterns, and behaviors related to a user's experience. The raw data from the study are the research findings. 

A list of findings, alone, is often not enough to fully communicate how users conceptualize a topic or why they experienced a certain struggle. Through analysis and synthesis of the data gathered in a research study, we are able to distill *insights* that connect the dots between related concerns and provide an additional layer of understanding. 

These synthesized research findings evolve into a cohesive collection of insights that enables stakeholders to make informed decisions. We support each insight with evidence that can be referenced during discussions, typically in the form of video clips, interview quotes, or statistical data.

### So, how does it work?
At the end of each research study, the study's moderator is responsible for documenting the research in the Insights Repository. We use GitLab issues to report on key findings from research studies. Every issue within the UXR_Insights repository contains a single insight on a particular topic. We connect a study's insights together by marking them as related, through GitLab's related issue functionality. Each issue is tagged with labels related to the feature area, type of research, and other relevant information that may help people find the issue.

Epics are used to organize issues from the same research study. The epic description should describe the research methodology used and any background information we have about the research participants. We tag all epics with the `~uxr_insights` label so they are easily found within the GitLab.org group.

A helpful approach to the documentation process is to start by creating an epic to house all of the insight issues. After creating the epic, it will be possible to use [quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html) to assign issues to the epic as you fill out the issue description.

When creating a new issue, guidelines for reporting insights can be found in the templates dropdown. These templates are there to help you see examples of how we document findings from studies that involve different research methods.  After you are finished adding context to an insights issue, it's best to close the issue so that it is clear that the work is complete.

One of the best ways to simplify the reporting process is to closely tie your note-taking approach to the research questions. You may find it useful to utilize this [notes template](https://docs.google.com/spreadsheets/d/1hnIqg-fnCYW2XKHR8RBsO3cYLSMEZy2xUKmbiUluAY0/edit#gid=0) for user interviews or the [Rainbow Spreadsheet](https://docs.google.com/spreadsheets/d/1j5mIFNWvHhxSUz0E1pXr1wGI385EDFc7uRtRHB_6ACc/edit#gid=3) approach for usability testing studies.

### Creating useful insights
It's important to remember that insights often need context, since people may read them in isolation and misinterpret them. As you work on reporting on your study, it's important to keep the audience in mind and think about what you'd like them to learn from the study.
 
For example, you may find it useful to document all insights (big or small) from studies conducted on the GitLab interface, in order to provide explanation for why the interface has changed over time. This creates a record of why previous design decisions were not ideal, which can be helpful for discussions with newer team members. On the other hand, for design evaluations or tests with prototypes, it may be more relevant to focus on documenting the insights that answer the overarching research questions, since these studies often result in additional design iterations before implementation. 

In either case, adding a brief "context" section at the top of the issue description and providing a link to the relevant design artifact can help the reader better understand the background of the study.

Here are some examples of useful insights created for common types of research studies:

- [Usability testing insight](https://gitlab.com/gitlab-org/uxr_insights/issues/571) from [SAST Setup research](https://gitlab.com/groups/gitlab-org/-/epics/1784)
- [User interview insight](https://gitlab.com/gitlab-org/uxr_insights/issues/842) from [Project & Portfolio Management research](https://gitlab.com/groups/gitlab-org/-/epics/2259)
- [Survey insight](https://gitlab.com/gitlab-org/uxr_insights/issues/706) from [Marketplace Nomenclature research](https://gitlab.com/groups/gitlab-org/-/epics/2138)
- [Icon testing insight](https://gitlab.com/gitlab-org/uxr_insights/issues/161) from ["Closed" Issue research](https://gitlab.com/groups/gitlab-org/-/epics/1286) 

### Leveraging the Insights Repository
Searching through the Insights Repository is a great way to get acquainted with GitLab users and learn about studies that have already been conducted. Since we use labels to tag insights, you can search and filter through issues to find the insights that are relevant to any stage, feature, and type of research.

Another approach is to look through the directory of completed research that is listed on the [UXR project README](https://gitlab.com/gitlab-org/uxr_insights#directory-of-completed-research), if you'd like a higher-level overview. These epics are typically labeled with `~uxr_insights`, to make it easier to access them.

Whether you're looking to better understand a user persona, learn about use cases for features, gather context for solving a problem, or plan a future research study - the Insights Repository has something for you.

## Templates

The following are examples of checklists that you may want to add to a research issue in order to keep track of what stage the research is up to.

#### User Interviews
```
* [ ] Product Manager: Draft the discussion guide.
* [ ] Product Designer or UX Researcher: Create the screening survey in Qualtrics.
* [ ] Product Designer or UX Researcher: Open a `Recruiting request` issue. Assign it to the relevant Research Coordinator.
* [ ] Research Coordinator: Recruit and schedule participants.
* [ ] Moderator: Invite the UX Research calendar and any other interested parties to the interviews.
* [ ] Moderator: Conduct the interviews.
* [ ] Moderator: Open an `Incentives request`. Assign it to the relevant Research Coordinator.
* [ ] Research Coordinator: Pay participants.
* [ ] Team: Analyze videos.
* [ ] Product Manager: Create issues in the UXR_Insights project documenting the findings.
* [ ] UX Researcher: Sense check the documented findings.
* [ ] UX Researcher: Update the `Problem validation` research issue. Link to findings in the UXR_Insights project. Unmark as `confidential` if applicable. Close issue.
```

#### Surveys
```
* [ ] Product Manager: Draft the survey.
* [ ] UX Researcher: Review the survey and provide feedback.
* [ ] Product Designer or UX Researcher: Transfer the survey questions to Qualtrics.
* [ ] Product Designer or UX Researcher: Open a `Recruiting request` issue. Assign it to the relevant Research Coordinator.
* [ ] Research Coordinator: Distribute the survey to a sample of participants.
* [ ] UX Researcher: Review responses received so far. Amend survey if necessary. Advise Research Coordinator to continue recruitment.
* [ ] UX Researcher: Notify Research Coordinator of survey closure.
* [ ] UX Researcher: Open an `Incentives request`. Assign it to the relevant Research Coordinator.
* [ ] Research Coordinator: Pay participants.
* [ ] UX Researcher: Analyze the data.
* [ ] UX Researcher: Create issues in the UXR_Insights project documenting the findings.
* [ ] UX Researcher: Share findings with Product Manager and Product Designer.
* [ ] UX Researcher: Update the `Problem validation` research issue. Link to findings in the UXR_Insights project. Unmark as `confidential` if applicable. Close issue.
```

#### Usability Testing
````
* [ ] Product Designer: Create a prototype.
* [ ] Product Designer or UX Researcher: Create the screening survey in Qualtrics.
* [ ] Product Designer or UX Researcher: Open a `Recruiting request` issue. Assign it to the relevant Research Coordinator.
* [ ] Product Designer: Draft the usability testing script.
* [ ] UX Researcher: Review the usability testing script and provide feedback.
* [ ] Product Designer: Invite the UX Research calendar and any other interested parties to the usability testing sessions.
* [ ] Product Designer: Conduct one usability testing session. Amend script if necessary.
* [ ] Product Designer: Conduct remaining usability testing sessions.
* [ ] Product Designer: Open an `Incentives request`. Assign it to the relevant Research Coordinator.
* [ ] Research Coordinator: Pay users.
* [ ] Team: Analyze videos.
* [ ] Product Designer: Create issues in the UXR_Insights project documenting the findings.
* [ ] UX Researcher: Sense check the documented findings.
* [ ] UX Researcher: Update the `Solution validation` research issue. Link to findings in the UXR_Insights project. Unmark as `confidential` if applicable. Close issue.
````

*Got further questions about UX Research? The UXR team is here for you! Reach out on the # ux_research slack channel.*
