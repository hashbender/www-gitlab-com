---
layout: handbook-page-toc
title: "Manager Toolkit"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The Manager Toolkit provides tools and resources to assist people managers (1+ direct report) in effectively leading and developing team members. The goal of the toolkit is to enhance clear communication, and increase team member engagement and retention, while leveraging best practices and shared learnings. The toolkit will continually evolve, and we invite you to contribute!

#### [Annual Compensation - Communication Guidelines](/handbook/people-group/manager-toolkit/compensation-review)