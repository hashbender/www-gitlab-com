---
layout: handbook-page-toc
title: Leadership Forum
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Leadership Forums provide opportunities for leaders to interact with peer groups in order to gain shared meaning and experiential learning on GitLab [leadership](/handbook/leadership/) skills through a [storytelling](https://www.sciencedirect.com/science/article/pii/S1048984307000744) format.
Each Leadership Forums is structured around one leadership topic and organized by leadership groups (e.g., Management Group, Director Group, S-Group, and E-group).

## Overview
Overview of the Leadership Forum design:
* Select a specific leadership topic (e.g., setting clear job expectations, managing performance, creating an inclusive environment) for the Leadership Forum.
* Participants take an online test (e.g., Google Form) that is created for that topic and based on material in our Handbook.
* Organize the live Leadership Forum for participants who have completed the test for that leadership topic. The Leadership Forum is guided by an individual at the next level above the participants' leadership group (e.g., E-group member guides S-Group Leadership Forum, and so on).
* A 25-minute Leadership Forum should have a maximum of 5 participants.

## Format

Format for 25 minute Leadership Forum session:
* 1 minute for quick introductions
* 5 minutes for participants to ask questions about the leadership topic (from the handbook or from the test)
* 5 minutes for one participant to share one personal story about the leadership topic
* 4 minutes total for other participants (1 minute each) to share their reactions to that story ("This reminds me of..." This feels like....) Note: At no point in the Leadership Forum should participants give advice or ask clarifying questions of individuals who are telling their story.
* 10 minutes for other personal stories (3 minutes each)

## Schedule

Leadership Forum Schedule:

*Forum 1*
* Participants: [S-Group](/handbook/leadership/#s-group)
* Guide: [E-Group](/handbook/leadership/#e-group) member
* Date: Q1 2020
* Topic: Setting clear job expectations to avoid [confusion about the expected output of each role](/handbook/leadership/biggest-risks/#confusion-about-the-expected-output).
* Prework: Prior to attending this forum, participants should familiarize themselves with the [confusion about the expected output](/handbook/leadership/biggest-risks/#confusion-about-the-expected-output) section of the handbook and then complete the online [test](https://forms.gle/sMFVXziKkHNLVDeS8).
