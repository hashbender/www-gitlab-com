--- 
layout: handbook-page-toc
title: "Investor Relations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Details TBA

## Earnings Release Calendar (day of quarter)
Day 7: Investor Relations (IR) Debrief

*  Attendees: CEO, CFO, and CLO
*  Discussion topic: Identify key developments from the prior quarter for inclusion in the shareholder letter

Day 10: Preliminary financial results and key metrics

*  Attendees: CFO, PAO, VP FP&A, and IR
*  Discussion topic: Drivers of financial and operating metric overperformance and underperformance to expectations

Day 21: Begin external audit process and legal review

Day 25: Disclosure Committee meeting

*  Attendees: CLO, PAO, VP of FP&A, and IR
*  Discussion topics: Approve shareholder letter, earnings release including final financial tables, investor presentation, and quarterly or annual SEC filing

Day 35: Release financial results including requisite SEC filings and host earnings call

*  Attendees: CEO, CFO, and IR
*  Discussion topic: Moderated question and answer session regarding past performance and outlook as of the reporting date  

Continuous updates: Abridged version of financials and key operating metrics, consensus, and Q&A tracker 

## Performance Indicator

### Enterprise Value to Sales
Enterprise Value to Sales compares the enterprise value (EV) of a company to its annual sales.

Enterprise Value to Sales = Enterprise Value/Annual Sales

Enterprise Value = Market Capitalization + Debt - Cash and Cash Equivalents
