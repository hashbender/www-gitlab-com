---
layout: job_family_page
title: "Payroll Specialist"
---

GitLab is looking for a highly motivated individual to join our Accounting team as a Payroll Specialist. The role will assist in the processing of multi-country and multi-state payrolls, maintaining related records, and developing and documenting various payroll policies and procedures.  We expect you to demonstrate the ability to work in a fast paced, both individually and as a part of a group, and thrive within a dynamic and rapidly changing environment.

### Levels

## Payroll Specialist - US

#### Responsibilities

- Own and process payroll, including tax changes, direct deposits, loan repayments, deduction goals, retroactive adjustments, prorated payments, bonuses, special pays, etc.
- Tax filings - Register new state/local/SUI tax account with local tax agency
- Process pension and benefit changes and funding
- Provide prompt, courteous, and efficient customer service to employees who have questions regarding their pay
- Review and approve expense reports
- Prepare ad hoc reports as needed
- Assist in the development and documentation of payroll procedures designed to streamline the process and scale with the organization
- Support internal and external payroll audit requests


#### Requirements

- 3-4  years' experience processing multi-state payroll
- International payroll experience is a plus
- Working knowledge of payroll best practices
- Strong knowledge of federal and state regulations
- Working knowledge of ADP and Excel
- Strong work ethic and team player
- High degree of professionalism
- Ability to deal sensitively with confidential material
- Strong verbal and written communication skills
- Decision-making, problem-solving, and analytical skills
- Organizational, multi-tasking, and prioritizing skills
- Ability to use GitLab

## Payroll Specialist - EMEA

#### Responsibilities

- Prepare and process payroll for non-US team members on a monthly basis for Netherlands, UK, Germany, and Australia
- Review and approve payroll invoices from PEO vendor(s) for payments
- Prepare and process contractor payments through iiPay
- Responsible for timely and accurate submission of all payroll data to required payroll vendors
- Verify and audit data processed by external payroll providers
- Provide timely and accurate response to team members’ payroll related queries
- Adhere to payroll procedures and align with relevant country law
- Assists with Year-End process
- Prepare ad-hoc reports as needed

#### Requirements

- 3-5 years of non-US payroll processing experience
- Previous experience of processing monthly payrolls in Europe
- Flexibility in dealing with multiple payrolls at one time
- Proficient with Google sheets (formula, pivot tablet, etc.)
- Ability to work in a collaborative environment
- Ability to collaborate and establish relationships cross-functionally with PeopleOps
- Excellent verbal and written communication skills
- Highly organized and attentive to detail when working to tight deadlines
- Demonstrate initiative and handle constantly evolving priorities and contending demands


## Performance Indicators
- [Payroll accuracy for each check date](/handbook/finance/payroll/#payroll-accuracy-for-each-check-date--100)
- [Payroll journal entry reports submitted to Accounting](/handbook/finance/payroll/#payroll-journal-entry-reports-submitted-to-accounting--payroll-date--2-business-days)


## Hiring process.

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their title on our team page.

- Selected candidates will be invited to schedule a screening call with a member of our Recruiting team
- Next, candidates will be invited to schedule a first interview with our Payroll and Payment Lead
- In addition, you may be asked to schedule time with our Senior Accounting and External Reporting Manager
- Next, candidates will be invited to schedule an interview with our Controller
- Candidates will then be invited to schedule an interview with our Senior People Operations Analysts
- Next, candidates will be invited to schedule an interview with our Chief Financial Officer
- Finally, candidates may be asked to interview with our CEO.

Additional details about our process can be found on our [hiring page](/handbook/hiring)
