import logging
from glob import glob

error_message = " job family not complete. Missing "
requirements = [
    "## Responsibilities",
    "## Requirements",
    "## Performance Indicators",
    "## Hiring Process",
    'Ability to use GitLab'
]

sales_requirements = [
    "## Responsibilities",
    "## Requirements",
    '<%= partial("job-families/sales/performance", :locals =>',
    "## Hiring Process",
    'Ability to use GitLab'
]

handbook_reference = ". See proper job family format at https://about.gitlab.com/handbook/hiring/job-families/#help-my-pipeline-is-failing"

exceptions = [
    "sales/professional-services-project-manager",
    "board-of-directors/board_member",
    "alliances/alliances-manager",
    "alliances/director-partnerships",
    "alliances/vp-of-alliances",
    "alliances/director-of-cloud-native-alliances",
    "product/pricing-manager",
    "product/corporate-development",
    "product/group-manager-product",
    "engineering/technical-writing-manager",
    "product/product-manager",
    "product/senior-director-of-product",
    "product/growth-product-manager",
    "product/vp-of-product-strategy",
    "product/product-operations",
    "product/vice-president-of-product",
    "engineering/technical-writer",
    "product/director-of-product",
    "marketing/marketing-program-manager",
    "marketing/product-marketing-manager-public-sector",
    "marketing/account-based-marketing-manager",
    "marketing/marketing-web-developer-designer",
    "marketing/evangelist-program-manager",
    "marketing/director-technical-evangelism",
    "marketing/pr-manager",
    "marketing/content-designer",
    "marketing/marketing-operations-manager",
    "marketing/marketing-full-stack-developer",
    "marketing/open-source-program-manager",
    "marketing/technical-marketing-manager",
    "marketing/brand-designer",
    "marketing/manager-marketing-programs",
    "marketing/developer-evangelist",
    "marketing/field-marketing-manager-public-sector",
    "marketing/vp-of-business-development-and-strategy",
    "marketing/global-content-manager",
    "marketing/digital-marketing-programs-manager",
    "marketing/community-advocate",
    "marketing/reference-program-manager",
    "marketing/sales-development-manager-commercial",
    "marketing/senior-sales-development-manager-acceleration",
    "marketing/education-program-manager",
    "marketing/senior-director-of-growth-marketing",
    "marketing/content-marketing",
    "marketing/chief-marketing-officer",
    "marketing/sales-development-manager",
    "marketing/director-corporate-marketing",
    "marketing/demand-generation-manager",
    "marketing/director-brand-and-digital",
    "marketing/marketing-intern",
    "marketing/global-programs-manager",
    "marketing/production-designer",
    "marketing/developer-marketing-manager",
    "marketing/director-of-sales-development",
    "marketing/customer-lifecycle-marketing-manager",
    "marketing/code-contributor-program-manager",
    "marketing/awareness-campaign-manager",
    "marketing/director-marketing-operations",
    "marketing/senior-director-of-revenue-marketing",
    "marketing/analysts-relations-manager",
    "marketing/director-field-marketing",
    "marketing/designer",
    "marketing/corporate-events-manager",
    "marketing/social-marketing-manager",
    "marketing/field-marketing-manager",
    "marketing/demand-generation-specialist",
    "marketing/product-marketing-manager",
    "marketing/culture-curator",
    "marketing/account-research-specialist",
    "marketing/director-of-community-relations",
    "marketing/product-marketing-management",
    "marketing/illustrator",
    "marketing/director-product-marketing",
    "marketing/marketing-operations-associate",
    "marketing/editor",
    "marketing/sales-development-representative",
    "marketing/digital-production-manager",
    "specialist/distributed-systems",
    "specialist/prometheus",
    "specialist/backstage",
    "finance/senior-internal-auditor",
    "finance/international-tax-manager",
    "finance/technical-accounting-manager",
    "finance/expense-specialist",
    "finance/accounting-manager",
    "finance/payroll-and-payments-lead",
    "finance/revenue-manager",
    "finance/data-engineer",
    "finance/accounts-payable-specialist",
    "finance/financial-analyst",
    "finance/vp-finance",
    "finance/accounting-operations-manager",
    "finance/it-helpdesk-analyst",
    "finance/procurement-manager",
    "finance/sox-compliance-analyst",
    "finance/accountant",
    "finance/finance-planning-and-analysis",
    "finance/director-of-tax",
    "finance/senior-director-data-and-analytics",
    "finance/business-system-analyst",
    "finance/internal-audit-manager",
    "finance/chief-financial-officer",
    "finance/it-operations-system-engineer",
    "finance/accounting-and-external-reporting-manager",
    "finance/pao-jf",
    "finance/finance-business-partner",
    "finance/manager-it",
    "finance/senior-director-of-investor-relations",
    "finance/finance-business-partner-sales",
    "finance/payroll-specialist",
    "finance/general-ledger-accountant",
    "finance/VP-Information-Systems",
    "expert/reliability",
    "expert/merge-request-coach",
    "legal/global-compliance-counsel",
    "legal/contract-manager",
    "legal/vp-legal-commercial-ip-compliance",
    "legal/director-contracts-legal-ops",
    "legal/global-compliance-manager",
    "legal/chief-legal-officer",
    "legal/paralegal",
    "legal/legal-intern",
    "people-ops/talent-operations-specialist",
    "people-ops/people-business-partner",
    "people-ops/chief-people-officer",
    "people-ops/candidate-experience-specialist",
    "people-ops/executive-business-administrator",
    "people-ops/recruiting-director",
    "people-ops/vp-recruiting",
    "people-ops/manager-recruiting-operations",
    "people-ops/recruiter",
    "people-ops/people-ops-fullstack-engineer",
    "people-ops/people-ops-specialist",
    "people-ops/people-ops-analyst",
    "people-ops/learning-development-specialist",
    "people-ops/web-content-manager",
    "people-ops/executive-recruiter",
    "people-ops/recruiting-sourcer",
    "people-ops/employment-branding-specialist",
    "people-ops/director-people-ops",
    "people-ops/diversity-inclusion-partner",
    "engineering/chief-technology-officer",
    "engineering/distinguished-engineer",
    "engineering/engineering-management-growth",
    "engineering/facility-security-officer",
    "engineering/support-management",
    "engineering/ux-research-manager",
    "engineering/general-manager-meltano",
    "engineering/frontend-engineer",
    "engineering/field-security-engineer",
    "engineering/monitoring-engineer",
    "engineering/fullstack-engineer",
    "engineering/support-engineer",
    "engineering/dotcom-support",
    "engineering/vulnerability-research-manager",
    "engineering/engineering-management-quality",
    "engineering/frontend-lead",
    "engineering/frontend-engineering-manager",
    "engineering/database-reliability-engineer",
    "engineering/ux-researcher",
    "engineering/vulnerability-research-engineer",
    "engineering/distinguished-engineer-secure",
    "engineering/database-engineer",
    "engineering/support-operations-specialist",
    "engineering/backend-engineer",
    "engineering/engineering-management-infrastructure",
    "engineering/threat-intelligence-engineer",
    "engineering/site-reliability-engineer",
    "engineering/monitoring-manager",
    "engineering/cloud-native-engineer",
    "engineering/operations-analyst",
    "engineering/security-management",
    "engineering/product-designer",
    "engineering/ux-management",
    "engineering/security-engineer",
    "sales/sales-operations",
    "sales/director-of-professional-services",
    "sales/director-sales-operations",
    "sales/sr-solutions-manager",
    "sales/director-of-sales-and-customer-enablement",
    "sales/japan-country-manager",
    "sales/sales-training-facilitator-sales-and-customer-enablement",
    "sales/vp-of-global-channels",
    "sales/area-sales-manager",
    "sales/director-federal-sales",
    "sales/vp-of-customer-success",
    "sales/sr-sales-operations-manager",
    "sales/sales-analytics-analyst-jf",
    "sales/federal-channel-manager",
    "sales/program-manager-sales-and-customer-enablement",
    "sales/senior-director-of-sales-operations",
    "sales/vp-of-commercial-sales",
    "sales/chief-revenue-officer",
    "sales/regional-sales-director",
    "sales/vp-enterprise-sales",
    "specialist/issue-triage",
]


def check_job_families(
    files, error_message, requirements, handbook_reference, exceptions
):
    # print(exceptions)
    for filename in files:
        # print(filename)
        job_family_name = filename.split("/index.html.md")[0]
        if job_family_name not in exceptions:
            with open(filename, "r") as stream:
                contents = stream.read()
                print(job_family_name)
                for requirement in requirements:
                    if requirement not in contents:
                        raise ValueError(
                            job_family_name
                            + error_message
                            + requirement
                            + handbook_reference
                        )
                    continue
            continue


if __name__ == "__main__":
    logging.basicConfig(level=20)
    logging.info("Starting job family check...")
    files = glob("*/*/index.html.md", recursive=True)
    # print(files)
    logging.info("Files collected... ")
    print(
        check_job_families(
            files,
            error_message,
            requirements,
            handbook_reference,
            exceptions
        )
    )
    sales_files = glob("*/*/index.html.md.erb", recursive=True)
    print(
        check_job_families(
            sales_files,
            error_message,
            sales_requirements,
            handbook_reference,
            exceptions,
        )
    )

    logging.info("Success.")
