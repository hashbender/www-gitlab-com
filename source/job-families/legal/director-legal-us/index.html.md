---
layout: job_family_page
Title: Director of Legal
---

The Director(s) of Legal are responsible for advising clients across GitLab on legal matters related to their functional area of expertise.
These are remote US-based roles to meet the requirement of a Juris Doctorate/United States law degree and bar admission in at least one US state.

## Responsibilities

* Partner with leadership, team members and other stakeholders across GitLab to address legal related matters
* Advise on, draft, maintain and review templates and policies
* Monitor and analyze changes in the law and legal risks; propose creative and effective solutions to address those risks
* Anticipate legal issues or risks and build the necessary processes and systems to address them
* Manage outside counsel relationships, as needed

## Requirements

* 8+ years combined law firm and in-house legal department experience, preferably in a multinational company
* Juris Doctorate/United States law degree and bar admission in at least one US state
* Proven success working effectively across business units and internal functions to efficiently resolve complex business issues
* Practical yet creative problem-solving approach that emphasizes addressing business needs while protecting GitLab’s interests
* Proactive, dynamic, and result driven self starter with strong attention to detail
* Familiarity working across the globe, to support multiple time zones and cultures
* Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization with a highly responsive and service-oriented attitude
* Sound and practical business judgment
* Previous experience in a remote work environment would be an added advantage
* Ability to use GitLab

## Specialities

### Corporate
The Director of Legal, Corporate collaborates on matters with clients across GitLab related to corporate governance and securities law compliance and related issues.

### Privacy and Product
The Director of Legal, Privacy and Product collaborates with clients across GitLab on a broad range of privacy and product matters.

### Employment
The Director of Legal, Employment collaborates with clients across GitLab on a broad range of employment law matters affecting our global team member base.

## Performance Indicators

### Corporate
* Timely and accurate contributions to filings and corporate governance matters
* Successful completion of M&A projects
* Accurate and complete corporate records

### Privacy and Product
* Successful oversight of privacy compliance initiatives
* Accurate export control administration
* Management of open source licensing directives

### Employment
* Timely and accurate contributions to global employment related matters
* Accurate and complete employment related templates, forms and policies
* Successful creation and administration of ongoing employment law related training for internal clients

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Selected candidates will be invited to schedule a 30-minute screening call with our Global Recruiters
* Next, candidates will be invited to schedule an interview with our Chief Legal Officer
* Then selected candidates will be invited to schedule with additional team members of the legal department
* Next, candidates will be invited to schedule interviews with directors and/or executives in relevant functional areas
* Finally, candidates may be required to meet with the CEO.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
