---
release_number: "X.Y" # version number - required
title: "GitLab X.Y released with Feature A and Feature B" # short title (no longer than 62 characters) - required
author: Joshua Lambert # author name and surname - required
author_gitlab: joshlambert # author's gitlab.com username - required
author_twitter: # author's twitter username - optional
image_title: '/images/X_X/X_X-cover-image.jpg' # cover image - required
description: "GitLab X.Y released with Feature A, Feature B, Feature C, Feature D and much more!" # short description - required
twitter_image: '/images/tweets/gitlab-X-Y-released.png' # social sharing image - not required but recommended
categories: releases # required
layout: release # required
featured: yes
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/#introduction
-->

Introduction.

[Markdown](/handbook/engineering/ux/technical-writing/markdown-guide/) supported.

Apply the class `{: .intro-header}` for `h2` headings and `{:.intro-header-h3}` for `h3`:

```md
## `h2` Heading
{: .intro-header}

### `h3` Heading
{:.intro-header-h3}
```

Which will render:

## Heading
{: .intro-header}

### `h3` Heading
{:.intro-header-h3}

<!--
Suggestion: describe each feature briefly in just a few words, using
anchors to link to their headings (use the relative path). The intro is supposed
to be eyes-catching, so "be happy" about it, describe them enthusiastically.
Focus on what are the advantages on having each of them. For some guidance,
look at the intros of past release posts.
-->

### Conclusion

<!--
Reminder: the final paragraph should include the total number of new features
being released, including bugs, performance improvements, and contributions from
non-DevOps stages like Enablement. All of these should be listed in the release
post, either as headers or bullet points.
-->

These are just a few highlights from the `A` new and improved features, and `B`
performance improvements described below, and a small selection from the `C`
merge requests made in X.Y. Check out more great updates below, such as...
